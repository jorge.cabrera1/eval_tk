#Evaluacion Tektom

#Api:

Source (mock)
http://demo1349271.mockable.io/categories/

Internal
http://localhost:8080/api/categories/


#Use 

"use the force" , or maven ...

´´´´
mvn spring-boot:run -Dspring-boot.run.profiles=dev

mvn spring-boot:run -Dspring-boot.run.profiles=prod

´´

* To demostrate the use of profiles, db files are different

#Examples:

'''

Products 

curl --location --request GET 'http://localhost:8080/api/categories/' 

curl --location --request GET 'http://localhost:8080/api/products/'

curl --location --request POST 'http://localhost:8080/api/products/' \
--header 'Content-Type: application/json' \
--data-raw '    {
        "categoryId": 1,
        "name": "Tokai Strat",
        "description": "SRV"
    }'

curl --location --request GET 'http://localhost:8080/api/products/'


curl --location --request PUT 'http://localhost:8080/api/products/2' \
--header 'Content-Type: application/json' \
--data-raw '    {
        "categoryId": 1,
        "name": "Tokai Strat",
        "description": "SRV"
    }'

'''

#Actuator
Responce time, health and default spring boot actuator are provided on /actuator

http://localhost:8080/actuator/metrics/http.server.requests
http://localhost:8080/actuator/loggers
http://localhost:8080/actuator/health


#Considerations:

* Categories are mocked using Mockable, rest url : http://demo1349271.mockable.io/categories/


* This uses H2 file database

