package com.tektom.evaluacion;

import com.tektom.evaluacion.entity.Category;
import com.tektom.evaluacion.services.CategoryService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @RequestMapping("/categories")
    public List<Category> getAllMovies(){
        List<Category> categories = categoryService.getCategories();
        return categories;
    }

}
