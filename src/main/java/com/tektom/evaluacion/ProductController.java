package com.tektom.evaluacion;

import com.tektom.evaluacion.entity.Category;
import com.tektom.evaluacion.entity.Product;
import com.tektom.evaluacion.services.CategoryService;
import com.tektom.evaluacion.services.ProductService;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ProductController {

  @Autowired ProductService productService;

  @RequestMapping("/products")
  public ResponseEntity<List<Product>> getAllMovies() {
    try {
      List<Product> products = productService.getProducts();
      return new ResponseEntity<>(products, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/products/{id}")
  public ResponseEntity<Product> getMovie(@PathVariable("id") long id) {
    Product product = productService.getProduct(id);

    if (product != null) {
      return new ResponseEntity<>(product, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @PostMapping("/products/")
  public ResponseEntity<Product> createTutorial(@RequestBody Product product) {
    try {
      Product respProduct = productService.saveProduct(product);
      return new ResponseEntity<>(respProduct, HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PutMapping("/products/{id}")
  public ResponseEntity<Product> updateTutorial(
      @PathVariable("id") long id, @RequestBody Product product) {
    Product currentProduct = productService.getProduct(id);

    if (currentProduct != null) {
      currentProduct.setName(product.getName());
      currentProduct.setDescription(product.getDescription());
      currentProduct.setCategoryId(product.getCategoryId());
      return new ResponseEntity<>(productService.saveProduct(currentProduct), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/products/{id}")
  public ResponseEntity<HttpStatus> deleteTutorial(@PathVariable("id") long id) {
    try {
      productService.deleteProducts(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
