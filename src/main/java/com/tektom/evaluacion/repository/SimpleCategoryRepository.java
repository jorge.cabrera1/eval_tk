package com.tektom.evaluacion.repository;


import com.tektom.evaluacion.entity.Category;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class SimpleCategoryRepository implements CategoryRepository{

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    ProductRepository productRepository;

    @Override
    @Cacheable("categories")
    public List<Category> getCategories(){
        ResponseEntity<Category[]> response =
            restTemplate.getForEntity(
                "http://demo1349271.mockable.io/categories/",
                Category[].class);
        Category[] categories = response.getBody();
        List<Category> m = Arrays.asList(categories);

        for (int i = 0 ;  i < m.size() ; i++) {
            Category c = m.get(i);
            c.setProductList(
                productRepository.findByCategoryId(c.getId())
            );
        }

        return  m;
    }

    @Override
    public Category getCategory(long id) {
        return null;
    }

}
