package com.tektom.evaluacion.repository;

import com.tektom.evaluacion.entity.Product;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
    List<Product> findByCategoryId(int categoryId);
}
