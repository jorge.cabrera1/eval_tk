package com.tektom.evaluacion.repository;

import com.tektom.evaluacion.entity.Category;
import java.util.List;

public interface CategoryRepository {
    List<Category> getCategories();
    Category getCategory(long id);
}
