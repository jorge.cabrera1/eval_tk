package com.tektom.evaluacion.services;


import com.tektom.evaluacion.entity.Category;
import com.tektom.evaluacion.repository.CategoryRepository;
import com.tektom.evaluacion.repository.ProductRepository;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CategoryService {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    CategoryRepository categoryRepository;

    public List<Category> getCategories(){
        return  categoryRepository.getCategories();
    }

}
