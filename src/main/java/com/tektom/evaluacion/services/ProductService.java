package com.tektom.evaluacion.services;


import com.tektom.evaluacion.entity.Category;
import com.tektom.evaluacion.entity.Product;
import com.tektom.evaluacion.repository.ProductRepository;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public Product getProduct(long id){
        return productRepository.getById(id);
    }

    public List<Product> getProducts() {
        return productRepository.findAll();
    }

    public Product saveProduct(Product product){
        return productRepository.save(product);
    }

    public void deleteProducts(long id) {
        productRepository.deleteById(id);
    }


}
