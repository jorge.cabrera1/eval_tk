drop table if exists product;

create table product (
  id int not null AUTO_INCREMENT,
  name varchar(100) not null,
  description varchar(100) not null,
  category_id int,
  PRIMARY KEY ( ID )
);
